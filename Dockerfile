#
# GitLab CI: Android
#
# https://gitlab.com/wldhx/gitlab-ci-android
#

FROM ubuntu:18.04
MAINTAINER Dmitriy Volkov <wldhx@wldhx.me>

ARG URL_SDK_TOOLS
ARG VERSION_BUILD_TOOLS
ARG VERSION_TARGET_SDK

ENV SDK_PACKAGES "build-tools;${VERSION_BUILD_TOOLS} platforms;android-${VERSION_TARGET_SDK} platform-tools extras;android;m2repository"

ENV ANDROID_HOME "/opt/android-sdk"
ENV PATH "$PATH:${ANDROID_HOME}/tools"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      curl \
      bsdtar \
      openjdk-8-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32ncurses5 \
      lib32z1 \
      openssh-client \
      git

RUN mkdir ${ANDROID_HOME} && \
    curl -s ${URL_SDK_TOOLS} | bsdtar -xf - -C ${ANDROID_HOME} && \
    find ${ANDROID_HOME}/tools -type f -exec chmod +x '{}' \;

# Use this if your lawyers aren't comfortable with auto-accepting licenses.
# Run ${ANDROID_HOME}/tools/bin/sdkmanager ${SDK_PACKAGES} manually and get the current hashes from $ANDROID_HOME/licenses/
#RUN mkdir -p $ANDROID_HOME/licenses/ && \
#    echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > $ANDROID_HOME/licenses/android-sdk-license && \
#    echo "84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_HOME/licenses/android-sdk-preview-license

RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses

RUN ${ANDROID_HOME}/tools/bin/sdkmanager ${SDK_PACKAGES}
